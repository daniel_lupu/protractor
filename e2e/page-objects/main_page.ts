import {by, element, protractor} from "protractor";

export class Main_page {

  // using method
  public navigationContent() {
    return element(by.id("navigation-content"));
  }

  public menuItem() {
    return element.all(by.id("navigation-content"));
  }
}
