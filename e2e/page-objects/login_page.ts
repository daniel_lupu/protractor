import {by, element, protractor} from "protractor";

export class LoginPage {

  public emailAddressField() {
    return element(by.id("Email"));
  }

  public passwordField() {
    return element(by.id("Password"));
  }

  public loginButton() {
    return element(by.id("loginButton"));
  }

}
