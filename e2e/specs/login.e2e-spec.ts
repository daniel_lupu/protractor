import {LoginPage} from "../page-objects/login_page";
import {Main_page} from "../page-objects/main_page";
import {browser, protractor} from "protractor";




describe('Login to eSignAnywhere', () => {
  let loginPage: LoginPage
  let mainPage: Main_page
  let EC = protractor.ExpectedConditions
  let userNameField
  let passwordField
  let loginButton
  let navigationContent

  beforeAll(async function() {
    browser.get('')
  });

  beforeEach(async function() {
    loginPage = new LoginPage();
    mainPage = new Main_page();
    userNameField = loginPage.emailAddressField();
    passwordField = loginPage.passwordField();
    loginButton = loginPage.loginButton();
    navigationContent = mainPage.navigationContent();
  });

  it('should login successfully' , async () => {
    browser.wait(EC.presenceOf(userNameField))
    userNameField.sendKeys(browser.params.userName)
    browser.wait(EC.visibilityOf(passwordField))
    passwordField.sendKeys(browser.params.password)
    expect(loginButton.isPresent()).toBeTruthy('Login button not present');
    loginButton.click();
    browser.wait(EC.visibilityOf(navigationContent))
    expect(navigationContent.isPresent()).toBeTruthy('Navigation Content is not present');
  });
});
