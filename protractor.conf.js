const JSONReporter = require('jasmine-json-test-reporter');
const { SpecReporter } = require('jasmine-spec-reporter');
const timeout = 30000;
const httpsUrl = 'https://qa-onprem.esignanywhere.net';
let reporters = require('jasmine-reporters');
let junitReporter = new reporters.JUnitXmlReporter({
  savePath: './reports/tmp',
  consolidateAll: true,
  filePrefix: 'protractor',
  stylesheetPath: '../../../../report.xsl'
});

exports.config = {
  plugins: [
    // failFast.init(),
  ],
  allScriptsTimeout: timeout,
  specs: [
     './e2e/specs/login.e2e-spec.ts',
  ],
  params: {
    sspFile : 'string',
    envelopeID: 'string',
    firstRedirectLink: 'string',
    pdfThumbprint: process.env.THUMBPRINT,
    userName: process.env.E2E_USER_NAME,
    password: process.env.JIRA_PASSWORD,
    logExtraSteps: false,
    baseUrl: httpsUrl,
    reportToJira: false,
  },
  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      'args': ["--disable-gpu", "--window-size=1200,1000", "lang=en-EN", "no-sandbox"]
    }
  },
  directConnect: true,
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  resultJsonOutputFile: './results.json',
  baseUrl: httpsUrl,
  framework: 'jasmine',
  jasmineNodeOpts: {
    stopSpecOnExpectationFailure: true,
    showColors: true,
    defaultTimeoutInterval: timeout,
    print: function () { }
  },
  beforeLaunch: function () {
    require('ts-node').register({
      project: 'tsconfig.e2e.json'
    });

  },

  afterLaunch: function () {

  },

  onPrepare: function () {
    jasmine.getEnv().addReporter(new JSONReporter({
      file: 'jasmine-test-results.json',
      beautify: true,
      indentationLevel: 4 // used if beautify === true
    }));
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: false } }));
    jasmine.getEnv().addReporter(junitReporter);
    browser.driver.manage().window().maximize();
    browser.waitForAngularEnabled(false);

    const process = require('process');
    const changedDir = 'e2e';
    console.log(process.cwd())
    console.log(changedDir)
    try {
      // Change working directory
      process.chdir(changedDir);
      console.log("Working directory after changing"
        + " directory: ", process.cwd());
    } catch (err) {
      // printing error if occurs
      console.error("error occured while changing"
        + " directory: ", err);
    }
  }
};

function buildUrl(tenant, domain) {
  return tenant + '.' + domain;
}
